# What to run
- Nats: `make nats`
- Nats init: `make run_init_docker`
- Seeding: in project [seed-realm](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/seed-realm) `cargo run -p seed_realm -- --nats-endpoint="nats://localhost:4222" --num-instances="1000"`
- Nats admin: `make nats_admin`, then in the container run `nats subscribe "events.>`
- Simulator: `make run`
- Clock: `make run_time_docker`, or use [SDG Orchestrator](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator)

# Nats

## Nats:
```sh
$ docker run --rm -it --name=nats --network=sdg -p=4222:4222 docker.io/library/nats --js
```

## Nats admin:
```sh
docker run --rm -it --network=sdg --entrypoint=/bin/sh docker.io/natsio/nats-box
nats context add nats --server nats --select
```

Init (automatically done by the init container of <https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator>):
```sh
nats stream add --subjects='events.>' --storage=memory --replicas=1 --retention=limits --discard=new --max-msgs=-1 --max-msgs-per-subject=-1 --max-bytes=128MB --max-age=1M --max-msg-size=1MB --dupe-window=1m --no-allow-rollup --deny-delete --no-deny-purge 'events'
nats stream add --subjects='time' --storage=memory --replicas=1 --retention=limits --discard=old --max-msgs=8 --max-msgs-per-subject=-1 --max-bytes=1KB --max-age=1M --max-msg-size=32 --dupe-window=1m --no-allow-rollup --no-deny-delete --no-deny-purge 'time'
```

purge:
```sh
nats stream purge events --force
```

subscribe:
```sh
nats subscribe  "events.frp.>"
```

reset consumer:
```sh
nats consumer rm sdg-event-simulator-handels-register
```

