use bevy::prelude::*;
use uuid::Uuid;

#[derive(Component)]
pub struct PersonId {
    pub uuid: Uuid,
    pub ssn: u64,
}
impl ToString for PersonId {
    fn to_string(&self) -> String {
        self.uuid.to_string()
    }
}
