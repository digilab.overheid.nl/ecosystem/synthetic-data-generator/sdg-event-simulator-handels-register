use bevy::prelude::*;
use uuid::Uuid;

#[derive(Component, Clone, Debug)]
pub struct PersonAddress {
    pub id: Uuid,
}
