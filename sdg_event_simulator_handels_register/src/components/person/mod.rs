use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};

mod address;
mod id;

pub use address::PersonAddress;
use chrono::Duration;
pub use id::PersonId;
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};

#[derive(Component)]
pub struct Person;

#[derive(Component)]
pub struct PersonHasCompany;

#[derive(Bundle)]
pub struct PersonBundle {
    pub person: Person,
    pub rng: RngComponent,
    pub id: PersonId,
}

#[derive(Component)]
pub struct PersonDied;

impl Person {
    pub fn creates_company(&self, rng: &mut RngComponent, clock_state: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);

        let chance = PercentageChance::new(0.1)
            .scale(Duration::hours(24), clock_state.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}
