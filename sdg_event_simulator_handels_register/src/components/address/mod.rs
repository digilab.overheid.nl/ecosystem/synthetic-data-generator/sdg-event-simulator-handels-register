use bevy::prelude::Component;
use uuid::Uuid;

pub type AddressId = Uuid;

#[derive(Component)]
pub struct HasBuilding(pub uuid::Uuid);

#[derive(Component, Clone)]
pub struct Address {
    pub id: AddressId,
    pub street: String,
    pub house_number: i32,
    pub house_number_addition: Option<String>,
    pub zip_code: String,
    pub municipality: (String, String),
    pub purpose: String,
    pub surface: i32,
}

impl Address {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        id: AddressId,
        street: String,
        house_number: i32,
        house_number_addition: Option<String>,
        zip_code: String,
        municipality: (String, String),
        purpose: String,
        surface: i32,
    ) -> Self {
        Self {
            id,
            street,
            house_number,
            house_number_addition,
            zip_code,
            municipality,
            purpose,
            surface,
        }
    }
}

#[derive(Component)]
pub struct AddressAvailable;

#[derive(Component)]
pub struct AddressClosed;
