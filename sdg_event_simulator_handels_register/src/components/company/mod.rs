use bevy::prelude::*;
use bevy_turborand::prelude::*;

mod id;
mod rsin;
mod statutory_name;

pub use id::CompanyId;
pub use rsin::Rsin;
pub use statutory_name::StatutoryName;

#[derive(Component)]
pub struct Company;

#[derive(Bundle)]
pub struct CompanyBundle {
    pub company: Company,
    pub rng: RngComponent,
    pub id: CompanyId,
}

impl CompanyBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>) -> Self {
        let mut rng = RngComponent::from(global_rng);
        let id = CompanyId::new(&mut rng);

        Self {
            company: Company,
            rng,
            id,
        }
    }
}
