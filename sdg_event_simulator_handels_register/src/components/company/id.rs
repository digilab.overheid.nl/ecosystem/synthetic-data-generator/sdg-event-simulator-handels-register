use ::uuid::{Builder as UuidBuilder, Uuid};
use bevy::prelude::Component;
use bevy_turborand::{DelegatedRng, RngComponent};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Component, Clone, Copy)]
pub struct CompanyId {
    pub uuid: Uuid,
}
impl ToString for CompanyId {
    fn to_string(&self) -> String {
        self.uuid.to_string()
    }
}
impl CompanyId {
    pub fn new(rng: &mut RngComponent) -> Self {
        let mut bytes = [0; 16];
        rng.fill_bytes(&mut bytes);
        let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

        Self { uuid: stable_uuid }
    }
}
