use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use uuid::Builder as UuidBuilder;

#[derive(Component)]
pub struct Rsin {
    pub rsin: String,
}
impl ToString for Rsin {
    fn to_string(&self) -> String {
        self.rsin.to_string()
    }
}

impl Rsin {
    pub fn new(rng: &mut RngComponent) -> Self {
        let mut bytes = [0; 16];
        rng.fill_bytes(&mut bytes);
        let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

        Self {
            rsin: stable_uuid.to_string(),
        }
    }
}
