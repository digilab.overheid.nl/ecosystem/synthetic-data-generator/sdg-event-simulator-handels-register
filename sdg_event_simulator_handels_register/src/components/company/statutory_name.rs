use std::fmt::Display;

use bevy::prelude::*;

#[derive(Component, Clone, Debug)]
pub struct StatutoryName {
    pub name: String,
}

impl Display for StatutoryName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl StatutoryName {
    pub fn new(name: String) -> Self {
        Self { name }
    }
}
