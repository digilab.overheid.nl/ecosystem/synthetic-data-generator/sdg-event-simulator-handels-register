use bevy::ecs::system::Resource;
use nats::jetstream::{ConsumerConfig, PullSubscribeOptions, PullSubscription};

#[derive(Resource)]
pub struct PersistantStream {
    pub stream: PullSubscription,
}

impl PersistantStream {
    pub fn new(uri: &str, ident: &str) -> Self {
        let stream = get_stream(uri, ident, "events.>");
        Self { stream }
    }
}

fn get_stream(nats_uri: &str, identity: &str, subject: &str) -> PullSubscription {
    let tnc = nats::connect(nats_uri).expect("Could not connect to nats");

    let options = nats::JetStreamOptions::new();
    let stream = nats::jetstream::JetStream::new(tnc, options);

    stream
        .pull_subscribe_with_options(
            subject,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                durable_name: Some(identity.to_string()),
                deliver_policy: nats::jetstream::DeliverPolicy::LastPerSubject,
                replay_policy: nats::jetstream::ReplayPolicy::Instant,
                filter_subject: "events.>".into(),
                ..Default::default()
            }),
        )
        .unwrap()
}
