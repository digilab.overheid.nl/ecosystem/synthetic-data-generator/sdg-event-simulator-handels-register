use bevy::prelude::*;
use nats::jetstream::BatchOptions;
use sdg_event_simulator_common::bevy::resources::nats_state::NatsState;

use crate::{
    components::{Address, AddressAvailable},
    event::{AddressRegisteredData, Event, ADDRESS_REGISTERED_TOPIC},
    resources::SeedState,
    systems::init::{get_seed_stream, FromSeed},
};

pub fn load_seed_addresses(
    mut commands: Commands,
    nats: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
) {
    info!("Load '{}' start", "seed address registered");

    let address_stream = get_seed_stream(&nats.uri, ADDRESS_REGISTERED_TOPIC);

    loop {
        let messages = address_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;

        for message in messages {
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();

            let event_data: AddressRegisteredData =
                serde_json::from_value(event.event_data).unwrap();

            let subject = event.subject_ids[0];

            if event_data.purpose != "Business" {
                continue;
            }

            let address = Address::new(
                subject,
                event_data.street,
                event_data.house_number,
                event_data.house_number_addition,
                event_data.zip_code,
                event_data.municipality,
                event_data.purpose,
                event_data.surface,
            );

            let entity = commands.spawn((address, FromSeed, AddressAvailable)).id();

            seed_state.push(subject, entity);

            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data addresses` end");
            break;
        }
    }
}
