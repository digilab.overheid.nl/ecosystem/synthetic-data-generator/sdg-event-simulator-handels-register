use bevy::prelude::*;
use nats::jetstream::{
    self, ConsumerConfig, JetStream, JetStreamOptions, PullSubscribeOptions, PullSubscription,
};

pub mod init_address;
pub mod init_person;

#[derive(Component)]
pub struct FromSeed;

pub fn get_seed_stream(nats_uri: &str, topic: &str) -> PullSubscription {
    let connection = nats::connect(nats_uri).expect("Could not connect to nats");
    let options = JetStreamOptions::new();
    let stream = JetStream::new(connection, options);

    stream
        .pull_subscribe_with_options(
            topic,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                deliver_policy: jetstream::DeliverPolicy::All,
                replay_policy: jetstream::ReplayPolicy::Instant,
                filter_subject: topic.to_string(),
                ..Default::default()
            }),
        )
        .unwrap()
}
