use bevy::prelude::*;
use bevy_turborand::{GlobalRng, RngComponent};
use nats::jetstream::BatchOptions;
use sdg_event_simulator_common::bevy::resources::nats_state::NatsState;

use crate::{
    components::{Person, PersonBundle, PersonId},
    event::{Event, EventBirthData, BIRTH_TOPIC, DEATH_TOPIC},
    resources::SeedState,
    systems::init::{get_seed_stream, FromSeed},
};

pub fn load_seed_births(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    nats_state: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
) {
    info!("Load `seed data birth` start");

    let birth_stream = get_seed_stream(&nats_state.uri, BIRTH_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventBirthData = serde_json::from_value(event.event_data).unwrap();
            let subject = event.subject_ids[0];

            // Create components
            let rng = RngComponent::from(&mut global_rng);
            let ssn = event_data.ssn;
            let id = PersonId { uuid: subject, ssn };

            let person = PersonBundle {
                person: Person,
                rng,
                id,
            };

            // Create entitiy
            let entity: Entity = commands.spawn((person, FromSeed)).id();
            // Update state

            seed_state.push(subject, entity);

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data birth` end");
            break;
        }
    }
}

pub fn load_seed_deaths(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    seed_state: Res<SeedState>,
) {
    info!("Load `seed data death` start");

    let birth_stream = get_seed_stream(&nats_state.uri, DEATH_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let subject = event.subject_ids[0];

            // Find entity
            let entity = seed_state.get(&subject).unwrap();

            // Remove entity
            commands.entity(entity).despawn();

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data death` end");
            break;
        }
    }
}
