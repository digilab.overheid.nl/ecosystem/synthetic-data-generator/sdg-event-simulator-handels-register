use bevy::prelude::*;
use bevy_turborand::prelude::*;
use nats::jetstream::BatchOptions;

use crate::{
    components::{
        Address, AddressAvailable, AddressClosed, Person, PersonBundle, PersonDied, PersonId,
    },
    event::{
        AddressRegisteredData, Event, EventBirthData, ADDRESS_REGISTERED_TOPIC,
        ADDRESS_WITHDRAW_TOPIC, BIRTH_TOPIC, DEATH_TOPIC,
    },
    resources::{PersistantStream, SeedState},
};

/// Get updates from other simulators and update local state
pub fn preupdate_loading(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    persistant_steam: Res<PersistantStream>,
    mut seed_state: ResMut<SeedState>,
) {
    loop {
        let batch = persistant_steam.stream.fetch(BatchOptions {
            batch: 1000,
            expires: None,
            no_wait: true,
        });

        let mut batch = match batch {
            Ok(batch) => batch,
            Err(err) => {
                debug!("ERROR Getting batch: {:?}", err);
                return;
            }
        };

        loop {
            let message = batch.next();

            let message = match message {
                Some(m) => m,
                None => {
                    return;
                }
            };

            let topic = message.subject;
            let data = std::str::from_utf8(&message.data).unwrap().to_string();

            match topic.as_str() {
                BIRTH_TOPIC => {
                    handle_birth(&mut global_rng, &mut commands, &mut seed_state, data);
                }
                DEATH_TOPIC => {
                    handle_death(&mut commands, &mut seed_state, data);
                }
                ADDRESS_REGISTERED_TOPIC => {
                    handle_register(&mut commands, &mut seed_state, data);
                }
                ADDRESS_WITHDRAW_TOPIC => {
                    handle_withdraw(&mut commands, &mut seed_state, data);
                }
                _ => {}
            }
        }
    }
}

fn handle_birth(
    global_rng: &mut ResMut<GlobalRng>,
    commands: &mut Commands,
    seed_state: &mut ResMut<SeedState>,
    data: String,
) {
    debug!("handle birth: {}", data);

    // handle data
    let event: Event = serde_json::from_str(&data).unwrap();
    let event_data: EventBirthData = serde_json::from_value(event.event_data).unwrap();
    let subject = event.subject_ids[0];

    // Create components
    let rng = RngComponent::from(global_rng);
    let ssn = event_data.ssn;
    let id = PersonId { uuid: subject, ssn };

    let person = PersonBundle {
        person: Person,
        rng,
        id,
    };

    // Create entitiy
    let entity: Entity = commands.spawn(person).id();

    // Update state
    seed_state.push(subject, entity);
}

fn handle_death(commands: &mut Commands, seed_state: &mut ResMut<SeedState>, data: String) {
    debug!("handle death: {}", data);

    // handle data
    let event: Event = serde_json::from_str(&data).unwrap();
    let subject = event.subject_ids[0];

    // Get subject id
    let subject_entity_id = seed_state.get(&subject);

    let subject_entity_id = if let Some(subject_entity_id) = subject_entity_id {
        subject_entity_id
    } else {
        return;
    };

    // Get subject
    let subject_entity = commands.get_entity(subject_entity_id);

    let mut subject_entity = if let Some(subject_entity) = subject_entity {
        subject_entity
    } else {
        return;
    };

    // Mark subject died
    subject_entity.insert(PersonDied);
}

fn handle_register(commands: &mut Commands, seed_state: &mut ResMut<SeedState>, data: String) {
    debug!("handle register: {}", data);

    // handle data
    let event: Event = serde_json::from_str(&data).unwrap();
    let event_data: AddressRegisteredData = serde_json::from_value(event.event_data).unwrap();
    let subject = event.subject_ids[0];

    if event_data.purpose != "Business" {
        return;
    }

    // Create components
    let address = Address::new(
        subject,
        event_data.street,
        event_data.house_number,
        event_data.house_number_addition,
        event_data.zip_code,
        event_data.municipality,
        event_data.purpose,
        event_data.surface,
    );

    // Create entitiy
    let entity = commands.spawn((address, AddressAvailable)).id();

    // Update state
    seed_state.push(subject, entity);
}

fn handle_withdraw(commands: &mut Commands, seed_state: &mut ResMut<SeedState>, data: String) {
    debug!("handle withdraw: {}", data);

    // handle data
    let event: Event = serde_json::from_str(&data).unwrap();
    let subject = event.subject_ids[0];

    // Get subject id
    let subject_entity_id = seed_state.get(&subject);

    let subject_entity_id = if let Some(subject_entity_id) = subject_entity_id {
        subject_entity_id
    } else {
        return;
    };

    // Get subject
    let subject_entity = commands.get_entity(subject_entity_id);

    let mut subject_entity = if let Some(subject_entity) = subject_entity {
        subject_entity
    } else {
        return;
    };

    // Mark address withdrawn
    subject_entity.insert(AddressClosed);
}
