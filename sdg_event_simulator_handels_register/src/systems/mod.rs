mod company;
mod init;
mod preupdate;

pub mod init_systems {
    pub use super::init::init_address;
    pub use super::init::init_person;
}

pub mod preupdate_systems {
    pub use super::preupdate::preupdate_loading;
}
pub mod update_systems {
    pub use super::company::{sim_address_closed, sim_company_registration, sim_owner_death};
}
