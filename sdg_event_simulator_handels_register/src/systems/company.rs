use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    event::{
        company_closed_event, company_registration_event, COMPANY_CLOSED_TOPIC,
        COMPANY_REGISTERD_TOPIC,
    },
    resources::SeedState,
};

/// Handle owner death
///
/// - Find owner child   -> company (Id for close event, despawn)
/// - Find owner child   -> address (Set available)
/// - Self (despawn)
#[allow(clippy::type_complexity)]
pub fn sim_owner_death(
    mut commands: Commands,
    mut owner_query: Query<(Entity, &mut RngComponent, &PersonId, &Children), With<PersonDied>>,
    query_company: Query<(Entity, &CompanyId)>,
    query_address: Query<(Entity, &Address)>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
) {
    for (owner_entity, mut rng, owner_id, children) in owner_query.iter_mut() {
        let mut company_id = None;
        let mut address_id = None;

        for child in children.iter() {
            let company = query_company.get_component::<CompanyId>(*child);
            let address = query_address.get_component::<Address>(*child);

            if let Ok(company) = company {
                company_id = Some(company.uuid);
                commands.entity(*child).despawn();
            } else if let Ok(address) = address {
                address_id = Some(address.id);
                commands.entity(*child).insert(AddressAvailable);
            } else {
                panic!("Person child should be either Company or Address ");
            }
        }

        seed_state.delete(&address_id.unwrap());
        seed_state.delete(&owner_id.uuid);

        commands.entity(owner_entity).despawn();

        let message = company_closed_event(&mut rng, &clock_state.get_time(), company_id.unwrap());

        nats_state
            .connection
            .publish(COMPANY_CLOSED_TOPIC, message.to_string())
            .unwrap();
    }
}

/// Handle office location closed
///
/// - Find address parent -> owner (Remove hasCompany component)
/// - Find owner child    -> company (Id for close event, despawn)
/// - Self (despawn)
#[allow(clippy::type_complexity)]
pub fn sim_address_closed(
    mut commands: Commands,
    mut owner_query: Query<(Entity, &mut RngComponent, &Children)>,
    query_company: Query<(Entity, &CompanyId)>,
    mut query_address: Query<(Entity, &Address, &Parent), With<AddressClosed>>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
) {
    for (address_entity, address, parent) in query_address.iter_mut() {
        let mut company_id = None;

        let owner_entity = parent.get();
        let (owner_entity, mut rng, owner_children) = owner_query.get_mut(owner_entity).unwrap();

        for child in owner_children.iter() {
            let company = query_company.get_component::<CompanyId>(*child);

            if let Ok(company) = company {
                company_id = Some(company.uuid);
                commands.entity(*child).despawn();
            }
        }

        seed_state.delete(&address.id);

        commands.entity(address_entity).despawn();
        commands.entity(owner_entity).remove::<PersonHasCompany>();

        let message = company_closed_event(&mut rng, &clock_state.get_time(), company_id.unwrap());

        nats_state
            .connection
            .publish(COMPANY_CLOSED_TOPIC, message.to_string())
            .unwrap();
    }
}

/// Create companies
///
/// - Check any living person, and any available address
/// - Create company (Create event)
/// - Set address to occupied
/// - Add company parent `person`
/// - Add company child `address`
#[allow(clippy::type_complexity)]
pub fn sim_company_registration(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    mut query_person: Query<(&mut RngComponent, Entity, &Person, &PersonId), Without<PersonDied>>,
    mut query_address: Query<(Entity, &Address), With<AddressAvailable>>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    // Find owner and address
    let mut address_options = query_address.iter_mut();

    for (mut rng, owner_entity, owner, owner_id) in query_person.iter_mut() {
        if owner.creates_company(&mut rng, &clock_state) {
            let address_option = address_options.next();
            let (address_entity, address_id) = match address_option {
                Some((entity, address)) => (entity, address.id),
                None => break,
            };

            // Create company
            let company_bundle = CompanyBundle::new(&mut global_rng);

            let rsin = Rsin::new(&mut rng);
            let statutionary_name = StatutoryName::new(format!("name-{}", rng.u32(0..u32::MAX)));

            // Create and publish event
            let message = company_registration_event(
                &mut rng,
                &clock_state.get_time(),
                company_bundle.id.uuid,
                owner_id.uuid,
                address_id,
                rsin.to_string(),
                statutionary_name.to_string(),
            );

            nats_state
                .connection
                .publish(COMPANY_REGISTERD_TOPIC, message.to_string())
                .unwrap();

            // Spawn company
            let company_entity = commands.spawn(company_bundle).id();

            // Set address to occupied
            commands.entity(address_entity).remove::<AddressAvailable>();
            commands.entity(owner_entity).insert(PersonHasCompany);

            // Add owner as company parent
            commands
                .entity(owner_entity)
                .push_children(&[company_entity]);

            // Add company as address parent
            commands
                .entity(owner_entity)
                .push_children(&[address_entity]);
        }
    }
}
