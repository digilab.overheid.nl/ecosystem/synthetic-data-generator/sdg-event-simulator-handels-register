use std::{env, process::exit};

use bevy::prelude::*;
use tracing_subscriber::{filter::LevelFilter, EnvFilter};

use sdg_event_simulator_common::{
    bevy::{
        plugins::SdgPluginGroup, resources::simulator_identity::SimulatorIdentity,
        run_loop::run_loop,
    },
    config::SdgConfig,
    flags::{setup_signals, wait_for_exit},
};

mod components;
mod event;
mod resources;
mod systems;

use crate::{
    resources::{PersistantStream, SeedState},
    systems::{init_systems, preupdate_systems, update_systems},
};

fn main() {
    let subscriber = tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .compact()
        .finish();
    tracing::subscriber::set_global_default(subscriber).unwrap();

    info!("Starting up simulator");

    setup_signals();

    let identity = "sdg-event-simulator-handels-register";
    App::new()
        .set_runner(run_loop)
        .add_plugins(SdgPluginGroup)
        .insert_resource(SimulatorIdentity::new(identity))
        .insert_resource(PersistantStream::new(&SdgConfig::nats_uri(), identity))
        .insert_resource(SeedState::default())
        .add_systems(Startup, init_systems::init_person::load_seed_births)
        .add_systems(Startup, init_systems::init_person::load_seed_deaths)
        .add_systems(Startup, init_systems::init_address::load_seed_addresses)
        .add_systems(PreUpdate, preupdate_systems::preupdate_loading)
        .add_systems(Update, update_systems::sim_address_closed)
        .add_systems(Update, update_systems::sim_owner_death)
        .add_systems(Update, update_systems::sim_company_registration)
        .run();

    let sleep_on_end = env::var("SLEEP").map(|v| v != "n").unwrap_or_else(|_| true);
    if sleep_on_end {
        wait_for_exit();
    }

    exit(0)
}
