use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use uuid::{Builder as UuidBuilder, Uuid};

pub use sdg_event_simulator_common::{config::SdgConfig, event::Event};

pub const BIRTH_TOPIC: &str = "events.frp.persoon.GeboorteVastgesteld";
pub const DEATH_TOPIC: &str = "events.frp.persoon.OverlijdenVastgesteld";

pub const ADDRESS_REGISTERED_TOPIC: &str = "events.frag.adres.Geregistreerd";
pub const ADDRESS_WITHDRAW_TOPIC: &str = "events.frag.adres.Intrekken";

pub const COMPANY_REGISTERD_TOPIC: &str = "events.fhr.bedrijf.Ingeschreven";
pub const COMPANY_CLOSED_TOPIC: &str = "events.fhr.bedrijf.Gesloten";

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventCompanyRegistrationData {
    pub id: Uuid,
    pub rsin: String,
    pub statutionary_name: String,
    pub owner_id: Uuid,
    pub address_id: Uuid,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventCompanyClosedData {
    pub id: Uuid,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventBirthData {
    #[serde(rename = "nationalInsuranceNumber")]
    pub ssn: u64,
    #[serde(rename = "placeOfBirth")]
    pub place_of_birth: (String, String),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventNameData {
    #[serde(rename = "givenName")]
    pub given_name: String,
    #[serde(rename = "surName")]
    pub surname: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventAddressRegistationData {
    pub address: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct AddressRegisteredData {
    #[serde(rename = "street")]
    pub street: String,
    #[serde(rename = "houseNumber")]
    pub house_number: i32,
    #[serde(rename = "houseNumberAddition")]
    pub house_number_addition: Option<String>,
    #[serde(rename = "zipCode")]
    pub zip_code: String,
    #[serde(rename = "municipality")]
    pub municipality: (String, String),
    #[serde(rename = "purpose")]
    pub purpose: String,
    #[serde(rename = "surface")]
    pub surface: i32,
}

pub fn company_registration_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    owner_id: Uuid,
    address_id: Uuid,
    rsin: String,
    statutionary_name: String,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventCompanyRegistrationData {
        id: subject_id,
        rsin,
        statutionary_name,
        owner_id,
        address_id,
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "BedrijfIngeschreven".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn company_closed_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventCompanyClosedData { id: subject_id };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "BedrijfGesloten".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}
