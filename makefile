SDG_NATS_SERVICE_HOST=0.0.0.0
SDG_NATS_SERVICE_PORT=4222
SLEEP="y"

# Develop apps
.PHONY: run
run:
	SDG_NATS_SERVICE_HOST="$(SDG_NATS_SERVICE_HOST)" SDG_NATS_SERVICE_PORT="$(SDG_NATS_SERVICE_PORT)" SLEEP=$(SLEEP) cargo run --bin sdg_event_simulator_handels_register

.PHONY: stop
stop:
	pkill -f target/debug/sdg_event_simulator_handels_register

.PHONY: lint
lint: 
	cargo machete
	cargo clippy

.PHONY: test
test:
	cargo test --all

# Docker
create_docker_network:
	docker network create sdg || true

# Nats
.PHONY: nats
nats: create_docker_network
	docker run --rm -it --name=nats --network=sdg -p=4222:4222 docker.io/library/nats --js

.PHONY: nats_admin
nats_admin: create_docker_network
	docker run \
		-it \
		--rm \
		--network=sdg \
		docker.io/natsio/nats-box \
		sh -c 'nats context add nats --server nats --select && sh'

# Build / Build test
.PHONY: build_docker
build_docker:
	docker build -f tools/app_container/Dockerfile --tag sdg_sim:latest .

.PHONY: run_docker
run_docker: create_docker_network
	docker run \
	-it \
	--rm \
	--name="sdg_sim" \
	--network=sdg \
	-e SLEEP='y' \
	-e SDG_NATS_SERVICE_HOST=nats \
	-e SDG_NATS_SERVICE_PORT='$(SDG_NATS_SERVICE_PORT)' \
	sdg_sim:latest


.PHONY: run_time_docker
run_time_docker: create_docker_network
	docker run \
	-it \
	--rm \
	--name="sdg_orchestrator" \
	--network=sdg \
	-e SDG_NATS_SERVICE_HOST=nats \
	-e SDG_NATS_SERVICE_PORT='$(SDG_NATS_SERVICE_PORT)' \
	-e SDG_SIMULATION_START=1990-01-01T00:00:00.00Z \
	-e SDG_SIMULATION_END=1992-01-01T00:00:00.00Z \
	-e SDG_SIMULATION_TIME_STEP=6h \
	-e SERVER_PORT=8080 \
	digilabpublic.azurecr.io/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator:main


.PHONY: run_init_docker
run_init_docker: create_docker_network
	docker run \
	-it \
	--rm \
	--name="sdg-orchestrator-init" \
	--network=sdg \
	-e SDG_NATS_SERVICE_HOST=nats \
	-e SDG_NATS_SERVICE_PORT='$(SDG_NATS_SERVICE_PORT)' \
	-e SDG_NATS_REPLICAS='1' \
	digilabpublic.azurecr.io/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator-init:main
